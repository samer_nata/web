//* ==== home page functions ==== 

window.addEventListener('scroll',function(){
    const header = this.document.querySelector('header');
    header.classList.toggle('sticky', window.scrollY > 0);
});

//* ==== add pateint page functions ====

function Restore(){
    //* ==== to restore input border color to it default state ====
    var temp = document.getElementsByClassName("form_input");
    for (var i = 0 ; i < temp.length ; i++){
        temp[i].style.border = '1px solid #f1f1f1';
    }
}
function CheckForm(){
    //* declare variables
    var message = '' , errorMessage = 0 ;

    var test =  document.getElementById('nationalNo').value ;
        //* ==== use regular expression pattern to check input value ====
    var ok = test.search(/^[01][0-9]{10}$/);
    
    if (ok != 0 ){
            //* ==== if there any error increase errorMessage variable by one to fire alert later ====
        errorMessage ++;
        message += 'the National Number should start with 0 or 1 and the lenght should be 11 digit \n';
            //* ==== make border in red color to get user attention where is the error ====
        document.getElementById('nationalNo').style.border = '1px solid red';
    }

    test = document.getElementById("clinicNo").value;
    ok = test.search(/^[0-9]{1,2}$/);
    if(ok != 0 ){
        errorMessage ++;
        message += 'only numbers between 0 and 99 are allow in Clinic number \n';
        document.getElementById('clinicNo').style.border = '1px solid red';
    }

    test = document.getElementById("address").value;
    ok = test.search(/[A-Za-z_0-9]/);
    if(ok != 0 ){
        errorMessage ++;
        message += 'only character and numbers are allowed \n';
        document.getElementById('address').style.border = '1px solid red';
    }

    //* fire message

    if (errorMessage == 0){
        return true
    }else {
        alert(message);
        return false;
    }
}







