﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add_patient_asp.aspx.cs" Inherits="Web_HomeWork_Aspt.pages_aps.add_patient_asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
  
        <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  
     <link rel="stylesheet" href="../css/add_patient.css"/>
    <title>Add Patient</title>
    <script type="text/javascript" src="../Scripts/script.js"></script>
  
</head>
<body>
  
    <asp:HyperLink runat="server" NavigateUrl="../pages_html/home_html.html">Back</asp:HyperLink>


     <section class="sign_up">

         <div class="container">
            <div class="sec_50">
                <form class="form" runat="server" defaultfocus=" "  >
                    <h2><span>S</span>ing up</h2>
      
                    <div class="form_div" >
                        <asp:TextBox ID="firstName" runat="server" AutoCompleteType="Disabled"  CssClass="form_input"   placeholder=" " ></asp:TextBox>
                        <asp:Label for="firstName" runat="server" Text="First" CssClass="form_label"></asp:Label>
           
                    </div>
                    
                    <div class="form_div">
                              <asp:TextBox ID="lastName" runat="server" AutoCompleteType="Disabled"  CssClass="form_input"  placeholder=" "></asp:TextBox>
                        <asp:Label for="lastName" runat="server" Text="Last Name" CssClass="form_label"></asp:Label>
                        
                    </div>
                    
                        <div class="form_div">
                            <asp:TextBox ID="nationalNo" runat="server" AutoCompleteType="Disabled" CssClass="form_input" placeholder=" "></asp:TextBox>
                            <asp:Label for="nationalNo" runat="server" Text="National Number"
                                CssClass="form_label"></asp:Label>

                            <br />
                            &nbsp;&nbsp;  &nbsp;&nbsp;  

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="nationalNo"
                                ForeColor="Red"
                                Display="Dynamic" ErrorMessage="Invalid National Number" Font-Size="8" ValidationExpression="^[01][0-9]{10}$" />


                        </div>
                   
                    <div class="form_div">
                               <asp:TextBox ID="clinicNo" runat="server" AutoCompleteType="Disabled"  CssClass="form_input"  placeholder=" "></asp:TextBox>
                        <asp:Label for="clinicNo" runat="server" Text="Clinic Number" CssClass="form_label"></asp:Label>
                        &nbsp;&nbsp;  &nbsp;&nbsp;  

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="clinicNo"
                                ForeColor="Red"
                                Display="Dynamic" ErrorMessage="Invalid clinic Number" Font-Size="8" ValidationExpression="^[0-9]{1,2}$" />
                    
                    </div>
                    <div class="form_div">

                      <asp:TextBox ID="address" runat="server" AutoCompleteType="Disabled"  CssClass="form_input"  placeholder=" "></asp:TextBox>
                        <asp:Label for="address" runat="server" Text="Address" CssClass="form_label"></asp:Label>
                                         &nbsp;&nbsp;  &nbsp;&nbsp;  

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="address"
                                ForeColor="Red"
                                Display="Dynamic" ErrorMessage="Invalid address" Font-Size="8" ValidationExpression="^[a-zA-Z0-9-]+[a-z A-Z 0-9 -]*$" />
                    
                     
                    </div>
                    <div class="form_div">

                        <asp:Label ID="Label1" runat="server" Text="Doctor Department: " CssClass="custom_lbl"></asp:Label>
                        <asp:DropDownList ID="Departments_Drop" runat="server" Width="120px" BackColor="#F6F1DB" ForeColor="#7d6754" Font-Names="Andalus" CssClass="ddl">
                            <asp:ListItem Value="Heart">Heart</asp:ListItem>
                            <asp:ListItem Value="Surgical"></asp:ListItem>
                            <asp:ListItem Value="Psychological">Psychological</asp:ListItem>
                        </asp:DropDownList>                       
                    </div>
                    <div class="form_div">
                         <asp:Label ID="Label2" runat="server" Text="Health Insurance" CssClass="custom_lbl"></asp:Label>
                       
                    <asp:CheckBox ID="Insurance_check" runat="server" CssClass="SingleCheckbox" />
                    </div>
                    <asp:Button ID="Button1" runat="server" Text="Sign up" CssClass="btn" OnClientClick="return CheckForm();" OnClick="insert_patient"/>
       
                </form>
                
            </div>
            <div class="sec_50">
                <div class="imgBx">

                    <asp:Image ID="Image1" runat="server" ImageUrl="../img/medicine.svg" AlternateText="sing up img" />


                </div>
            </div>
        </div>
    </section>
    


       

</body>


</html>
