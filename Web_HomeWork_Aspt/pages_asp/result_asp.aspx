﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="result_asp.aspx.cs" Inherits="Web_HomeWork_Aspt.pages_aps.result_asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


        <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Result</title>
    <link rel="stylesheet" href="../css/result.css"/>
</head>
<body>
   
        <asp:HyperLink runat="server" NavigateUrl="add_patient_asp.aspx" >Back</asp:HyperLink>
        <asp:HyperLink runat="server" NavigateUrl="../pages_html/home_html.html">Home</asp:HyperLink>
    <div class="container">
        
        <asp:Image CssClass="success_img" ID="Image1" runat="server" ImageUrl="../img/checkmarksuccess.gif" AlternateText="404 not Found"/>
        <h2>Add patient successfuly</h2>
    </div>
</body>
</html>
